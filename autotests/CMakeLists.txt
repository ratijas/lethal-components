# SPDX-FileCopyrightText: 2024 ivan tkachenko <me@ratijas.tk>
#
# SPDX-License-Identifier: BSD-2-Clause

if(NOT TARGET Qt6::QuickTest)
    message(STATUS "Qt6QuickTest not found, autotests will not be built.")
    return()
endif()

add_executable(qmltest qmltest.cpp)
target_link_libraries(qmltest PRIVATE
    Qt6::Qml
    Qt6::QuickTest
)
target_link_libraries(qmltest PRIVATE
    Kirigami
)

macro(lethal_add_tests)
    if (WIN32)
        set(_extra_args -platform offscreen)
    endif()

    set(_extra_args ${_extra_args} -import ${CMAKE_BINARY_DIR}/bin)

    foreach(test ${ARGV})
        add_test(NAME ${test}
            COMMAND qmltest
                   ${_extra_args}
                   -input ${test}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )
    endforeach()
endmacro()

lethal_add_tests(
    tst_sample.qml
)
