/*
 *  SPDX-FileCopyrightText: 2024 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

import QtQuick
import QtTest
import tk.ratijas.lethal.models as LethalModels

TestCase {
    id: root

    name: "SampleTest"
    visible: true
    when: windowShown

    width: 500
    height: 500

    Component {
        id: sampleComponent
        LethalModels.Sample {}
    }

    Component {
        id: sampleNativeComponent
        LethalModels.SampleNative {}
    }

    function test_sample(): void {
        const sample = createTemporaryObject(sampleComponent, this);
        verify(sample);
    }

    function test_native(): void {
        const sample = createTemporaryObject(sampleNativeComponent, this);
        verify(sample);
    }
}
