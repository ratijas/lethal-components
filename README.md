# Lethal Components

A personal collections of various QML/QtQuick libraries.

## Build

This is generic CMake project based on [ECM](https://invent.kde.org/frameworks/extra-cmake-modules).

It can be built and installed by [kdesrc-build](https://invent.kde.org/sdk/kdesrc-build).
To use it add the following module definition to your ~/.config/kdesrc-buildrc:

```
module lethal-components
    repository git@invent.kde.org:ratijas/lethal-components.git
    branch master
end module
```
