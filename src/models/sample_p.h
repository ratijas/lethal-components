/*
 *  SPDX-FileCopyrightText: 2024 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include <QObject>
#include <qqml.h>

class Sample : public QObject
{
    Q_OBJECT
    QML_NAMED_ELEMENT(SampleNative)

public:
    explicit Sample(QObject *parent = nullptr);
    ~Sample() override;
};
