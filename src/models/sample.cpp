/*
 *  SPDX-FileCopyrightText: 2024 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "sample_p.h"
#include "lethal-components-models-logging.h"

Sample::Sample(QObject *parent)
    : QObject(parent)
{
    qCWarning(LethalModels) << "Created Sample";
}

Sample::~Sample()
{
}

#include "moc_sample_p.cpp"
