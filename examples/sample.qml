/*
 *  SPDX-FileCopyrightText: 2024 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

import QtQml
import tk.ratijas.lethal.models as LethalModels

LethalModels.Sample {
    Component.onCompleted: {
        print("sample", sample);
    }
}
